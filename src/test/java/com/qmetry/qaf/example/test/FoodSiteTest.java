package com.qmetry.qaf.example.test;

import static com.qmetry.qaf.automation.step.CommonStep.*;
import static com.qmetry.qaf.example.steps.StepsLibrary.searchFor;
import static com.qmetry.qaf.automation.step.CommonStep.*;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

public class FoodSiteTest extends WebDriverTestCase {

	public void recipeslinkTest(String linkText) {
		getDriver().findElement(By.linkText(linkText)).click();
		verifyLinkWithPartialTextPresent("BANANA");
	}

	public void recipeofDay(String linkText) throws InterruptedException {
		//getDriver().findElement(By.className("rib-text"));
		
		//getDriver().findElement(By.xpath("//div[@class='fd-inner-tile']//div[@class='rib-text']")).getText();
		getDriver().findElement(By.xpath("//div[@class='rib-text']")).getText();
		Thread.sleep(2000);
		getDriver().findElement(By.xpath(linkText)).click();
		//getDriver().navigate().back();
	}

	public void facebookTest(String linkText) {
		getDriver().findElement(By.xpath(linkText)).click();
	}

	public void checkSearch(String searchKey, String searchValue) {
		getDriver().findElement(By.xpath(searchKey)).click();
		System.out.println("Search box clicked");
		getDriver().findElement(By.id("search-input")).clear();
		getDriver().findElement(By.id("search-input")).sendKeys(searchValue);
		getDriver().findElement(By.xpath("//div[@class='search-results-heading container']")).getText();
	}

	public void invalidSearch(String searchValue) throws InterruptedException {
		getDriver().findElement(By.id("search-input")).clear();
		getDriver().findElement(By.id("search-input")).sendKeys(searchValue);
		verifyLinkWithPartialTextPresent("No");
		getDriver().findElement(By.xpath("//div[@class='search-results-heading container']")).getText();
		Thread.sleep(2000);
	}

	public void linksofPage(String inputParameter) {
		List<WebElement> links = getDriver().findElements(By.tagName("a"));

	}

	public void checkforOptions(String inputParameter) {
		String outputText = getDriver()
				.findElement(By.xpath("//div[@class='tile-filters magic-buttons']//label[@class='js-selected']"))
				.getText();
	}

	public void popularlinkTest(String linkText) {
		getDriver().findElement(By.linkText(linkText)).click();
	}

	public void checkforPopularText(String inputParameter) {
		WebElement ribbon = getDriver().findElement(By.xpath(inputParameter));
		ribbon.click();
		String Text = ribbon.getText();
		Assert.assertEquals(Text, "TOP-RATED RECIPES", "Popular Text Matched");
	}

	public void headerValidation(String linkXPath, String headerXPath) {
		getDriver().findElement(By.xpath(linkXPath)).click();

		String Texth1 = getDriver().findElement(By.xpath(headerXPath)).getText();

		String Expectedh1 = "OUR 109 MOST-TWEAKED RECIPES";
		Assert.assertEquals(Texth1, Expectedh1, "header Matched");
		String pageTitle = getDriver().getTitle();

		String expectedTitle = "Most Tweaked Recipes - Food.com";
		Assert.assertEquals(pageTitle, expectedTitle, "Title Matched");
	}

	public void facebooktesting(String xPath) throws InterruptedException {
		WebElement facebook = getDriver().findElement(By.xpath(xPath));
		facebook.click();
		System.out.println("facebook link clicked");

		Set<String> allWindowHandles = getDriver().getWindowHandles();

		for (String handle : allWindowHandles) {
			getDriver().switchTo().window(handle);
			System.out.println(getDriver().getTitle());
			if (getDriver().getTitle().equalsIgnoreCase("Facebook")) {

				FaceBookTest fb = new FaceBookTest();
				fb.FBTest("", "");
				//fb.FBTest("XYZ", "XYZ");

			}
			System.out.println("Window handle - > " + handle);
			Thread.sleep(8000);
			
			getDriver().navigate().to("https://www.food.com/ideas/tacos-and-burritos-6570#c-596571");
		
			//getDriver().switchTo().defaultContent();
		}
	}

	// click on the email link in top Recipes and enter the email id

	public void emailClick() throws InterruptedException {
		getDriver().findElement(By.linkText("POPULAR")).click();
		Thread.sleep(3000);
		WebElement email = getDriver()
				.findElement(By.xpath("//a[@class='gk-share-link global-share email']//i[@class='icon-fdc-email']"));
		email.click();
		System.out.println("Email clicked");

	}

	public void springCookingTest(String inputParameter) throws InterruptedException {
		getDriver().findElement(By.linkText("SPRING COOKING")).click();
		getDriver().findElement(By.linkText(inputParameter)).click();
		Thread.sleep(3000);

	}

	public void springLink() throws InterruptedException {
		List<WebElement> links = getDriver().findElements(By.tagName("a"));

		for (int i = 0; i < links.size(); i++) {
			if ((links.get(i).getText()).equalsIgnoreCase("Spring")) {
				links.get(i).getText();
			}
		}
	}

	public void seasonalCooking(String inputParameter) throws InterruptedException {
		getDriver().findElement(By.xpath(inputParameter)).click();
		List<WebElement> downloadHeader = getDriver().findElements(By.xpath(inputParameter));
		if (downloadHeader.size() > 0) {
			System.out.println("Found h2 header Downloads");
		}

		getDriver().findElement(By.xpath("//div[@class='smart-card-inner']/aside"));
		getDriver().findElement(By.xpath("//div[@class='smart-card-inner']")).getAttribute("innerhtml");

	}

	public void summerTextValidation(String inputParameter) throws InterruptedException {
		getDriver().findElement(By.linkText("SUMMER")).click();

		String summertitle = getDriver().getTitle();

		String expectedTitle = "Seasonal Cooking: Summer Recipes, Ideas And More - Food.com";

		Assert.assertTrue(summertitle.equalsIgnoreCase(expectedTitle),
				"Page title not matched or Problem in loading url page");
	}

	public void footerValidation(String inputParameter) throws InterruptedException {
		getDriver().findElement(By.id(inputParameter)).sendKeys("jfkdsjflsdjflsdjfisjreiwjrewkmrlewkrewrewrewrwerewr");
		verifyLinkWithPartialTextPresent("No");
		System.out.println("No results found for the search input: ");
	}

}

/*
 * @Test public void test1() throws InterruptedException { get("/");
 * 
 * // go to Recipes link click(("RECIPES"));
 * getDriver().findElement(By.linkText("RECIPES")).click();
 * //verifyLinkWithPartialTextPresent("BANANA");
 * 
 * // get the text of Recipe of the day link String Text =
 * getDriver().findElement(By.xpath("//div[@class='rib-text']")).getText();
 * System.out.println(" Text is.." + Text);
 * getDriver().findElement(By.xpath("//div[@class='rib-text']")).click();
 * 
 * // finding facebook link on the page and clicking it
 * getDriver().findElement(By.xpath(
 * "//i[@class='icon-fdc-facebook']//following::input")).click();
 * System.out.println("Facebook link clicked");
 * System.out.println(getDriver().getTitle());
 * 
 * // dropdown /* WebElement check =
 * getDriver().findElement(By.xpath("//input[@id='magic-chkbox']")); for (int
 * i=0;i<5;i++) { check.click(); System.out.println(check.isSelected()); }
 */

// Click Search Icon and enter text
/*
 * getDriver().findElement(By.xpath("//i[@class='icon-gk-search']")).click();
 * System.out.println("Search box clicked"); // clear the search box area
 * getDriver().findElement(By.id("search-input")).clear(); // sendKeys("Banana",
 * // getDriver().findElement(By.id("search-input")).toString());
 * getDriver().findElement(By.id("search-input")).sendKeys("Banana");
 * Thread.sleep(5000);
 * 
 * /* List <WebElement> RelatedSearches = getDriver().findElements(By.
 * xpath("//div[@id='search-related']//div[@class='search-items search-suggest-items']//ul[@class='search-items-list search-suggest-items-list']"
 * )); List <WebElement> RelatedSearches = getDriver().findElements(By.
 * xpath("//div[@class='search-items search-suggest-items']//ul[@class='search-items-list search-suggest-items-list']"
 * )); getDriver().findElements(By.
 * xpath("/div[@class='search-items search-suggest-items']");
 * System.out.println("Length of related Searches: " +RelatedSearches.size());
 * for(WebElement e : RelatedSearches) { System.out.println(e.getText()); }
 */
// Testing with invalid search
/*
 * getDriver().findElement(By.id("search-input")).clear();
 * getDriver().findElement(By.id("search-input")).sendKeys("89099000");
 * Thread.sleep(3000); // verifyLinkWithPartialTextPresent("No");
 * System.out.println("No results found for the search input: ");
 * 
 * // Getting all the links in the page List<WebElement> links =
 * getDriver().findElements(By.tagName("a")); System.out.println(links.size());
 * for (int i = 1; i <= links.size(); i = i + 1) {
 * System.out.println(links.get(i).getText()); }
 * 
 * // checkbox getDriver().findElement(By.linkText("RECIPES")).click();
 * List<WebElement> checkboxes =
 * getDriver().findElements(By.xpath("//input[@id='magic-chkbox']")); for
 * (WebElement checkbox : checkboxes) { System.out.println(checkbox.getText());
 * // verifyLinkWithPartialTextPresent("New");
 * 
 * }
 * 
 * 
 * }
 * 
 * 
 * }
 */

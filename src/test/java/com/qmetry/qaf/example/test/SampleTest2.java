package com.qmetry.qaf.example.test;

import static com.qmetry.qaf.automation.step.CommonStep.*;
import static com.qmetry.qaf.example.steps.StepsLibrary.searchFor;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

@Test

public class SampleTest2 extends WebDriverTestCase {

	public void test2() throws InterruptedException {

		get("/");

		getDriver().findElement(By.linkText("POPULAR")).click();
		Thread.sleep(3000);
		// verify the link Top Rated recipes text
		WebElement ribbon = getDriver().findElement(By.xpath("//div[@class='top-ribbon-panel']"));
		ribbon.click();
		String Text = ribbon.getText();
		System.out.println(" Text is.." + Text);

		// Click on TopMost Recipes link and assert the title and header of the page
		WebElement TopMost = getDriver().findElement(By.xpath("//a[text()='108 Most-Tweaked Recipes']"));
		TopMost.click();
		WebElement headerh1 = getDriver()
				.findElement(By.xpath("//div[contains(@class,'smart-content-heading-inner')]/h1"));
		String texth1 = headerh1.getText();
		String Expectedh1 = "OUR 109 MOST-TWEAKED RECIPES";
		Assert.assertEquals(texth1, Expectedh1, "header Matched");
		System.out.println("Header h1  " + texth1);
		String pageTitle = getDriver().getTitle();
		System.out.println("Page Title is " + pageTitle);
		String expectedTitle = "Most Tweaked Recipes - Food.com";
		Assert.assertEquals(pageTitle, expectedTitle, "Title Matched");

		// click on facebook link with some text in email and password field:

		WebElement facebook = getDriver().findElement(
				By.xpath("//a[@class='gk-share-link global-share facebook']//i[@class='icon-fdc-facebook']"));
		facebook.click();
		System.out.println("facebook link clicked");

		Set<String> allWindowHandles = getDriver().getWindowHandles();
		int i = 0;
		for (String handle : allWindowHandles) {
			getDriver().switchTo().window(handle);
			System.out.println(getDriver().getTitle());
			if (getDriver().getTitle().equalsIgnoreCase("Facebook")) {

				FaceBookTest fb = new FaceBookTest();
				fb.FBTest("", "");
				fb.FBTest("XYZ", "XYZ");

			}
			System.out.println("Window handle - > " + handle);
			Thread.sleep(3000);
              getDriver().switchTo().defaultContent();
		}

		// saves link validation:

				getDriver().findElement(By.xpath("//i[@class='icon-gk-save']")).click();
				Thread.sleep(3000);
				WebElement edit = getDriver()
						.findElement(By.xpath("//article[@class='icon actions--right__search-icon']//following::button[1]"));
				// WebElement edit = getDriver().findElement(By.xpath("//article[@class='icon
				// actions--right__search-icon']//following::button[@class='btn btn-edit
				// btn-link']"));
				edit.click();
				System.out.println("Edit link clicked.");
				// getDriver().switchTo().frame(getDriver().findElement(By.id("overlay")));
				// getDriver().findElement(By.xpath("//div[@id='gigya-modal-plugin-container-showScreenSet_content']//input[@type='submit']")).click();
		
				// click on the email link in top Recipes and enter the email id

		getDriver().findElement(By.linkText("POPULAR")).click();
		Thread.sleep(3000);
		WebElement email = getDriver()
				.findElement(By.xpath("//a[@class='gk-share-link global-share email']//i[@class='icon-fdc-email']"));
		email.click();
		System.out.println("Email clicked");

	}

}

package com.qmetry.qaf.example.test;

import static com.qmetry.qaf.automation.step.CommonStep.*;
import static com.qmetry.qaf.automation.step.CommonStep.verifyLinkWithPartialTextPresent;
import static com.qmetry.qaf.example.steps.StepsLibrary.searchFor;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

public class SampleTestSuite extends WebDriverTestCase {

	@Test
	public void test1() throws InterruptedException {
		get("/");
		FoodSiteTest fbt = new FoodSiteTest();

		// Clicking on Receipe
		fbt.recipeslinkTest("RECIPES");

		// Check for dropdown checkbox (validate the option that is selected in the dropdown "Reccommended")
		fbt.checkforOptions("//input[@id='magic-chkbox']");

		// Going into recipe of the day page (Clicking on the Recipe of the Day tag)
		fbt.recipeofDay("//div[@class='rib-text']");

		// Sharing RecipeOftheDay with FB (Click on the FB link )
		//fbt.facebookTest("//a[@class='gk-share-link global-share facebook']");
		fbt.facebooktesting("//a[@class='gk-share-link global-share facebook']");

		// Checking the valid search (Valid search under Recommended)
		fbt.checkSearch("//i[@class='icon-gk-search']", "Orange");

		Thread.sleep(8000);

		// Checking the invalid Search (Invalide search under Recommended)
		fbt.invalidSearch("8888");
		Thread.sleep(2000);
		
		// Checking the links on the page (Get all links of page in Recipes)
		fbt.linksofPage("a");

		Thread.sleep(8000);

		// Check for Spring cooking test
		fbt.springCookingTest("SPRING COOKING");
		Thread.sleep(2000);
		
		// get all the links in the page that has text Spring in the tagname
		fbt.springLink();
		Thread.sleep(2000);
		
		//Get all the links wrapped in the seasonal cooking
		fbt.seasonalCooking("//a[contains(text(),'Seasonal Cooking')]");
		Thread.sleep(2000);
		
		//validate the page title when summer link is clicked
		fbt.summerTextValidation("Summer");
		Thread.sleep(2000);
		
		
		fbt.footerValidation("footer-search-field");

		// Click on Popular Link
		fbt.popularlinkTest("POPULAR");
		
		//Validate the Top Rated Recipes link
		fbt.checkforPopularText("//div[@class='top-ribbon-panel']");
		Thread.sleep(8000);
      
		//header validation in Popular link
		fbt.headerValidation("//a[text()='108 Most-Tweaked Recipes']",
				"//div[contains(@class,'smart-content-heading-inner')]/h1");

		Thread.sleep(8000);
		

		// email click
		fbt.emailClick();

	}

	/*
	 * @Test(priority=2) public void test2() throws InterruptedException { get("/");
	 * FoodSiteTest fbt = new FoodSiteTest(); fbt.recipeslinkTest("RECIPES");
	 * fbt.recipeofDay("//div[@class='rib-text']");
	 * fbt.facebookTest("//i[@class='icon-fdc-facebook']//following::input");
	 * fbt.checkSearch("//i[@class='icon-gk-search']", "Banana");
	 * fbt.invalidSearch("8888"); }
	 */

	// go to Recipes link click(("RECIPES"));
	// getDriver().findElement(By.linkText("RECIPES")).click();
	// verifyLinkWithPartialTextPresent("BANANA");

	// get the text of Recipe of the day link
	/*
	 * String Text =
	 * getDriver().findElement(By.xpath("//div[@class='rib-text']")).getText();
	 * System.out.println(" Text is.." + Text);
	 * getDriver().findElement(By.xpath("//div[@class='rib-text']")).click();
	 * 
	 * // finding facebook link on the page and clicking it
	 * getDriver().findElement(By.xpath(
	 * "//i[@class='icon-fdc-facebook']//following::input")).click();
	 * System.out.println("Facebook link clicked");
	 * System.out.println(getDriver().getTitle());
	 */
	// }
}
// dropdown
/*
 * WebElement check =
 * getDriver().findElement(By.xpath("//input[@id='magic-chkbox']")); for (int
 * i=0;i<5;i++) { check.click(); System.out.println(check.isSelected()); }
 */

// Click Search Icon and enter text
/*
 * getDriver().findElement(By.xpath("//i[@class='icon-gk-search']")).click();
 * System.out.println("Search box clicked"); // clear the search box area
 * getDriver().findElement(By.id("search-input")).clear(); // sendKeys("Banana",
 * // getDriver().findElement(By.id("search-input")).toString());
 * getDriver().findElement(By.id("search-input")).sendKeys("Banana");
 * Thread.sleep(5000);
 * 
 * /* List <WebElement> RelatedSearches = getDriver().findElements(By.
 * xpath("//div[@id='search-related']//div[@class='search-items search-suggest-items']//ul[@class='search-items-list search-suggest-items-list']"
 * )); List <WebElement> RelatedSearches = getDriver().findElements(By.
 * xpath("//div[@class='search-items search-suggest-items']//ul[@class='search-items-list search-suggest-items-list']"
 * )); getDriver().findElements(By.
 * xpath("/div[@class='search-items search-suggest-items']");
 * System.out.println("Length of related Searches: " +RelatedSearches.size());
 * for(WebElement e : RelatedSearches) { System.out.println(e.getText()); }
 */
// Testing with invalid search
/*
 * getDriver().findElement(By.id("search-input")).clear();
 * getDriver().findElement(By.id("search-input")).sendKeys("89099000");
 * Thread.sleep(3000); verifyLinkWithPartialTextPresent("No");
 */
// System.out.println("No results found for the search input: ");

// Getting all the links in the page
/*
 * List<WebElement> links = getDriver().findElements(By.tagName("a"));
 * System.out.println(links.size()); for (int i = 1; i <= links.size(); i = i +
 * 1) { System.out.println(links.get(i).getText()); }
 */

// checkbox
/*
 * getDriver().findElement(By.linkText("RECIPES")).click(); List<WebElement>
 * checkboxes =
 * getDriver().findElements(By.xpath("//input[@id='magic-chkbox']")); for
 * (WebElement checkbox : checkboxes) { System.out.println(checkbox.getText());
 * verifyLinkWithPartialTextPresent("New");
 */

// }

/*
 * } }
 */
